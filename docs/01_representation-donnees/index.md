---
author: Sophie CHANE-LUNE
title: Objectifs
---

# REPRÉSENTATION DES DONNÉES

1. [**Représentation binaire d’un entier**](./representation-entier-positif.md)

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Écriture d’un entier positif dans une base b ⩾ 2.
        - Passer de la représentation d’une base dans une autre. 

2. **Représentation binaire d’un entier relatif**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Représentation binaire d’un entier relatif.
        - Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entiers. 
        - Utiliser le complément à 2. 

3. **Représentation approximative des nombres réels**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Représentation approximative des nombres réels : notion de nombre flottant.
        - Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3.

4. **Expressions booléennes**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Valeurs booléennes : 0, 1.
        - Opérateurs booléens : and, or, not.
        - Expressions booléennes.
        - Dresser la table d’une expression booléenne. 

5. **Représentation d’un texte en machine**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Représentation d’un texte en machine.
        - Exemples des encodages ASCII, ISO-8859-1, Unicode .
        - Identifier l’intérêt des différents systèmes d’encodage. 
        - Convertir un fichier texte dans différents formats d’encodage. 

6. **Tuples**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - p-uplets.
        - p-uplets nommés.
        - Écrire une fonction renvoyant un p-uplet de valeurs.

7. **Tableaux**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Tableau indexé.
        - Tableau donné en compréhension.
        - Lire et modifier les éléments d’un tableau grâce à leurs index. 
        - Construire un tableau par compréhension. 
        - Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]. 
        - Itérer sur les éléments d’un tableau. 

8. **Dictionnaires**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Dictionnaires par clés et valeurs.
        - Construire une entrée de dictionnaire. 
        - Itérer sur les éléments d’un dictionnaire. 

