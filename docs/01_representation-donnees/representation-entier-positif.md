---
author: Sophie CHANE-LUNE
title: 1. Représentation des nombres entiers positifs
---
# REPRÉSENTATION DES NOMBRES ENTIERS POSITIFS

## Introduction
Les ordinateurs et les programmes que l'on utilise tous les jours permettent de mémoriser, de transmettre et de transformer des nombres, des textes, des images, des sons, etc. Pourtant, quand on les observe à une plus petite échelle, ces ordinateurs ne manipulent que des $0$ et des $1$. Mémoriser, transmettre et transformer des nombres, des textes, des images ou des sons demande donc d'abord de les représenter comme des suites de $0$ et de $1$, c'est-à-dire qu'il faut convenir d'un **codage** de ces informations en **binaire**.

La mémoire des ordinateurs est constituée d'une multitude de petits circuits électroniques qui ne peuvent être, chacun, que dans deux états : $0$ et $1$. Une telle valeur, $0$ ou $1$, s'appelle un **booléen**, un **chiffre binaire** ou encore un **bit** (*Binary digIT*). 

!!! history "Histoire de l'informatique"
    Ce nom vient du mathématicien britannique [George Boole](https://fr.wikipedia.org/wiki/George_Boole){:target="_blank"} (XIXème siècle) qui a créé l’algèbre booléenne, fondement de la logique booléenne.

    <center>
    ![George Boole](../04_architecture-materielle/images/05_george-boole_min.png "George Boole")
    </center>

Un tel circuit à deux états s'appelle un circuit mémoire un bit et son état se décrit donc par le symbole $0$ ou par le symbole $1$. L'état d'un circuit, composé de plusieurs de ces circuits mémoire un bit, se décrit par une suite finie de $0$ et de $1$, que l'on appelle un **mot**. Par exemple, le mot $100$ décrit l'état d'un circuit composé de trois circuits mémoire un bit, respectivement dans l'état $1$, $0$ et $0$.

## I. Système de numération positionnelle
### a. Système décimal
Depuis le Moyen-Age, on écrit les nombres entiers naturels en *notation décimale à position*. Nous y sommes tellement habitués que nous confondons un nombre avec sa représentation. En effet, un entier naturel s'écrit en notant, de droite à gauche, le nombre d'unité, le nombre de paquets de dix, le nombre de paquets de cent, le nombre de paquets de mille, etc. Chacun de ces nombres étant compris entre zéro et neuf, seuls dix chiffres ou symboles  sont nécessaires : $0$, $1$, $2$, $3$, $4$, $5$, $6$, $7$, $8$ et $9$. Par exemple, l'écriture $2048$ exprime un entier naturel formé de $8$ unités, $4$ dizaines, $0$ centaine et $2$ milliers. La notation décimale à position s'appelle donc aussi la notation à position en base dix. 

$2048 = 2\times10^{3} + 0\times10^{2} + 4\times10^{1} + 8\times10^{0}$

Mais on aurait pu tout aussi bien décider de faire des paquets de deux, de cing, de douze, de seize, de soixante, etc. On écrirait alors les nombres entiers naturels en notation à position en base deux, cinq, douze, seize ou soixante. Par conséquent, un nombre peut cependant être représenté dans n'importe quelle base $B$, en utilisant $B$ symboles différents pour les chiffres.
En notation binaire, c'est-à-dire en notation à position en base deux, le nombre treize s'écrit $1101$: de droite à gauche, $1$ unité, $0$ deuzaine, $1$ quatraine et $1$ huitaine. 

!!! warning "Remarques"
    - Pour exprimer un nombre dans une base différente de dix, on indique la base en indice : par exemple, $1101_{2}$ (en base deux).
    - Pour facililiter la lecture, on rassemble parfois les bits par groupe de quatre ou de huit dans les mots très longs : par exemple, $1111111101_{2}$ s'écrit $11$ $1111$ $1101_{2}$. 

### b. Système binaire
### c. Système hexadécimal
### d. Système en base B

## II. Conversion
### a. Conversion de la base 10 en base 2
2.1.1 Méthode 1
uio
2.1.2 Méthode 2
uio
### b. Conversion de la base 2 en base 10
2.2.1 Méthode 1
uio
2.2.2 Méthode 2
uio
### c. Conversion de la base 10 en base 16
essai
essai

## III. Addition binaire
