---
author: Sophie CHANE-LUNE
title: Objectifs
---

# LANGAGE & PROGRAMMATION

1. **Initiation à la programmation**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Constructions élémentaires.
        - Mettre en évidence un corpus de constructions élémentaires.

2. **Utilisation de bibliothèques**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Utilisation de bibliothèques.
        - Utiliser la documentation d’une bibliothèque.

3. **Spécification et mise au point de programme**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Spécification.
        - Prototyper une fonction. 
        - Décrire les préconditions sur les arguments. 
        - Décrire des postconditions sur les résultats.

        - Mise au point de programmes.
        - Utiliser des jeux de tests.

4. **Les langages de programmation**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Diversité et unité des langages de programmation.
        - Repérer, dans un nouveau langage de programmation, les traits communs et les traits particuliers à ce langage.
