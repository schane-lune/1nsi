---
author: Sophie CHANE-LUNE
title: 1. Architecture matérielle
---
# ARCHITECTURE MATÉRIELLE

## Introduction
Un ordinateur est une machine de traitement de l’information. L’information qu’il manipule est stockée dans la mémoire, et les traitements qu’il effectue sont réalisés par le processeur. Ces traitements sont également stockés dans la mémoire, sous forme de programme. C’est l’originalité de cette architecture qui est due à John von Neumann, qui lui a donné son nom. Un ordinateur a également un ou plusieurs périphériques pour communiquer avec l’extérieur tels que l’écran, le clavier, la mémoire de masse (disque dur) ou l’interface réseau (Internet).

## I. Les composants d'un ordinateur
![Les composants d'un ordinateur](./images/01_composants.png "Les composants d'un ordinateur")

### a. La carte mère
!!! abstract "Définition"
    La **carte mère** joue un rôle fondamental dans la structure des ordinateurs. C'est elle qui accueille l'ensemble des composants internes de l’ordinateur (processeur, mémoire, …) et gère les différentes interfaces avec les périphériques (prise pour les éléments internes et ports USB pour les périphériques externes). 

Concrètement, il s’agit d’une carte électronique permettant d’interconnecter tous les circuits imprimés d’un ordinateur entre eux. C’est la plus grosse carte de l’ordinateur qui va centraliser toutes les données et les faire traiter par le processeur.

### b. Le processeur
!!! abstract "Définition"
    Le **processeur** ou Unité Centrale de Traitement (UCT) (en anglais CPU, Central Processing Unit) est un composant essentiel qui exécute les instructions machine des programmes informatiques. Sa vitesse est mesurée en Hertz (MHz ou GHz), indiquant combien d'opérations il peut effectuer par seconde.

- Architecture monoprocesseur :  
Dans une architecture monoprocesseur, une seule séquence d'instructions est exécutée à la fois par le processeur.
- Architecture multiprocesseur :  
Dans une architecture multiprocesseur, plusieurs séquences d'instructions sont exécutées en parallèle, une par processeur.

### c. La mémoire
!!! abstract "Définition"
    La **mémoire** permet de stocker des données et des programmes. Sa capacité est mesurée en gigaoctets (Go) ou téraoctets (To).

#### Mémoire volatile
!!! abstract "Définition"
    La mémoire volatile permet de stocker temporairement les données et les programmes en cours de fonctionnement. Elle s'efface quand la machine n'est plus alimentée en électricité, favorisant ainsi un accès rapide aux informations, ce qui améliore les performances du système pour les tâches courantes.

!!! example "Exemples"
    - Mémoire vive  
    La mémoire vive, RAM (Random Access Memory) ou SRAM (Static RAM), est un type spécifique de mémoire utilisée par les ordinateurs pour stocker temporairement les données et les programmes en cours d'utilisation. Elle permet un accès rapide aux informations, ce qui améliore les performances du système pour les tâches courantes. Dans les ordinateurs personnels, la mémoire vive a une capacité de 4 à 32 Go.

    - Mémoire cache  
    La mémoire cache est un type de mémoire très rapide située entre le processeur et la mémoire vive. Elle est conçue pour stocker temporairement les données et les instructions les plus fréquemment utilisées par le processeur afin d'accélérer les performances globales de l'ordinateur. La mémoire cache réduit le temps d'accès aux données en évitant que le processeur ait à récupérer ces informations directement depuis la RAM, qui est plus lente. Elle a une capacité d'une dizaine de mégaoctets.

    - Registre  
    Un registre est un type de mémoire très rapide et de petite capacité située directement dans le processeur. Les registres sont utilisés pour stocker temporairement des données et des instructions pendant que le processeur exécute des opérations. Ils jouent un rôle crucial dans l'exécution des instructions car ils permettent un accès extrêmement rapide aux données nécessaires pour les calculs et les opérations logiques. Elle a une capacité d'une dizaine d'octets.

#### Mémoire non volatile
!!! abstract "Définition"
    La mémoire non volatile conserve les données même lorsque l'alimentation électrique est coupée. 

!!! example "Exemples"
    - Mémoire morte  
    La mémoire morte ou ROM (Read-Only Memory) est un type de mémoire principalement utilisée en lecture seule, pour stocker des informations permanentes essentielles au démarrage et au fonctionnement de base de l'appareil.

    - Mémoire de masse  
    La mémoire de masse désigne les dispositifs de stockage de grande capacité utilisés pour conserver les données à long terme. C'est le cas
    des disques durs SSD, cartes SD, clés USB. La taille mémoire est actuellement de plusieurs téraoctets.

    - Mémoire flash  
    La mémoire flash est un type de mémoire non volatile qui peut être effacée électriquement et reprogrammée. Elle est utilisée dans divers dispositifs de stockage et de mémoire, connue pour son accès rapide aux données.

### d. Les périphériques
!!! abstract "Définition"
    Un **périphérique** est un dispositif connecté à une machine, qui lui ajoute des fonctionnalités et lui permet d'intéragir avec l'utilisateur.

#### Périphérique d'entrée
Un périphérique d'entrée permet à la machine d'obtenir des informations, comme par exemple un clavier, une souris, un touchpad, un micro ou un scanner.

#### Périphérique de sortie
Un périphérique de sortie permet d'envoyer des informations à l'extérieur de la machine, comme par exemple un écran, des haut-parleurs, une imprimante.

#### Périphérique d'entrée/sortie 
Un périphérique d'entrée/sortie est un périphérique qui permet à la fois de lire des informations (périphérique d'entrée) et d'en écrire (périphérique de sortie), comme par exemple un disque dur, la carte réseau.

#### Carte d'extension
Une carte d'extension est composée de circuits électroniques (nappes, pistes de circuits imprimés, etc.) qui apporte des fonctionnalités supplémentaire à une machine. Elle est connectée à la carte mère par un bus, comme par exemple la carte graphique ou la carte réseau.

### e. Les bus
!!! abstract "Définition"
    Un **bus** est un dispositif de transmission de données ou un câble de liaison entre les composants d'une machine.

### f. Une alimentation
!!! abstract "Définition"
    Elle fournit en électricité tous les composants de l'ordinateur.

## II. Architecture de von Neumann
### a. Un processeur pour calculer et une mémoire pour stocker programme et données
L’**architecture de von Neumann** est un modèle structurel d’ordinateur dans lequel une unité de stockage (mémoire) unique sert à conserver à la fois les instructions et les données demandées ou produites par le calcul. Les ordinateurs actuels sont tous basés sur des versions améliorées de cette architecture datant de 1945.

!!! abstract "Définition"
    L’architecture de von Neumann décompose l’ordinateur en 4 parties distinctes :

    - l’**unité arithmétique et logique** (UAL ou ALU en anglais) ou unité de traitement effectue les opérations de base (elle contient un registre appelé accumulateur noté ACC) ;  
    - l’**unité de contrôle** ou de commande (UC) (en anglais CU, Control Unit) est chargée du « séquençage » des opérations (le programme lui indique à quelle adresse sont stockées les données dans la mémoire et quels sont les calculs à effectuer) ;  
    - la **mémoire** contient à la fois les données et le programme qui indiquera à l’unité de contrôle quels sont les calculs à faire sur ces données ainsi que les résultats à stocker ;  
    - les **dispositifs d’entrée-sortie** permettent de communiquer avec le monde extérieur.

Les instructions exécutées sont en **langage machine** dit de **bas niveau**.

!!! warning "Remarque"
    De nos jours, le processeur est constitué d'une unité arithmétique et logique (UAL) et d'une unité de contrôle.

![Architecture de von Neumann](./images/02_von-neumann.png "Architecture de von Neumann")

La mémoire et le processeur communiquent par l’intermédiaire de 3 bus :

- le bus d’adresses qui permet au processeur d’indiquer à la mémoire l’emplacement qu’il souhaite accéder pour lire ou écrire une donnée ;

- le bus de données qui transporte les données du processeur vers la mémoire (écriture) ou de la mémoire vers le processeur (lecture) ;

- et enfin le bus de contrôle qui permet de coordonner le fonctionnement du processeur et de la mémoire.

Le schéma général de l’architecture est le suivant :  
![Architecture de von Neumann](./images/03_von-neumann-bus.png "Architecture de von Neumann")

### b. Cycle d'exécution d'une instruction
Un ordinateur est une machine programmable, c’est-à-dire que l’on peut définir, dans un programme stocké en mémoire, la séquence d’instructions qu’il doit effectuer. L’unité de contrôle est chargée d’obtenir les instructions successives du programme et de les exécuter. Pour cela, elle contient deux registres : le compteur de programme (CP), qui contient l’adresse en mémoire de la prochaine instruction à exécuter, et le registre d’instruction (RI), qui contient l’instruction en cours d’exécution. Cette instruction est constituée d’un code opération (OP) et d’un opérande qui est en général une adresse (ADR).  

L’unité de contrôle contient aussi une horloge qui permet de séquencer ses actions. C’est la vitesse de cette horloge qui détermine la vitesse d’exécution du programme. Cette vitesse est limitée d’une part par la vitesse à laquelle les composants électroniques réagissent, d’autre part par leur consommation électrique.

![Cycle d'exécution d'une instruction](./images/04_cycle-execution.png "Cycle d'exécution d'une instruction")

!!! gear "Cycle d'exécution d'une instruction"
    Le **cycle d'exécution** d'une instruction décrit le processus par lequel un ordinateur traite et exécute les instructions d'un programme. Ce cycle peut être divisé en plusieurs étapes :  

    1. Le processeur récupère dans la mémoire l'instruction suivante à exécuter.  
        - l’adresse contenue dans le compteur de programme (CP) est envoyée à la mémoire pour récupérer dans le registre d’instruction (RI) l’instruction à exécuter ;  
        - le compteur de programme est incrémenté de 1 pour être prêt à aller chercher la prochaine instruction au prochain cycle.  

    2. L'instruction récupérée est déchiffrée par le processeur.  
        l’instruction est analysée et décomposée en un code opération (OP), envoyé à l’unité arithmétique et logique.

    3. Le processeur exécute l'instruction décodée. Cela peut impliquer des opérations arithmétiques ou logiques, des transferts de données, ou des modifications de l'état du processeur. Selon le code opération, plusieurs cas sont possibles :  
        - pour une opération arithmétique ou logique (addition, soustraction, comparaison), l’adresse ADR est envoyée à la mémoire et la donnée lue est envoyée à l’UAL, qui effectue l’opération OP avec le contenu de l’accumulateur et y stocke le résultat ;  
        - lorsque l’opération porte sur un nombre au lieu d’une adresse ADR, ce nombre est transféré directement à l’UAL par le bus de données ;  
        - pour une opération de saut (JMP pour « jump »), l’adresse ADR est transférée dans le compteur de programme (CP), ce sera donc la prochaine instruction, qui sera exécutée.  

    4. Si l'instruction implique un accès à la mémoire (par exemple, une opération de chargement ou de stockage), le processeur accède à la mémoire pour lire ou écrire les données nécessaires :  
        - pour une opération de lecture (LOD pour « load »), l’adresse ADR est envoyée à la mémoire et la donnée lue est envoyée à l’UAL ;  
        - pour une opération d’écriture (STO pour « store »), l’adresse ADR et le contenu de l’accumulateur ACC sont envoyés à la mémoire pour écriture ;  
        
    Le cycle peut alors recommencer avec l’instruction suivante, et ainsi de suite.

## III. Instructions machines et assembleur

## IV. Circuit combinatoire et logique booléenne

## Conclusion

---
**_Références :_**

- *NSI 1re Spécialité - Ed. 2021, Éditions Hachette Éducation*
