---
author: Sophie CHANE-LUNE
title: Activité 3 Linux ... une interface en ligne de commande
---

Pour interagir avec l'utilisateur, les systèmes d'exploitation ont une interface graphique (comme par exemple Windows) et une **interface en ligne de commande** pour saisir en texte les commandes que le système d'exploitation doit effectuer.  
Avec Linux, comme dans Windows, les données sont stockées dans des fichiers, eux-mêmes contenus dans des dossiers appelés **répertoires**.  
En revanche, il n'existe pas de notion de "disque" sous Linux. L'organisation des principaux fichiers et répertoires appelée **arborescence** est représentée en version simplifiée ci-dessous :
![Arborescence Linux](../images/act3_arborescence.png){ width=50%; : .center }

## A. Système de fichiers de Linux
Dans le système de fichiers de Linux, tous les répertoires nécessaires au fonctionnement du système d'exploitation (`etc`, `dev`, `tmp`, ...) sont accessibles depuis le répertoire racine `/`.  
Les répertoires des deux utilisateurs de cette machine sont situés dans le répertoire `home`, ici il s'agit de `Angie`et `Matéi`.  

!!! warning "Attention"
        Le système d'exploitation Linux est sensible à la casse. Cela signifie qu'il fait une distinction entre les lettres majuscules et les lettres minuscules, et qu'il ne traite donc pas de la même façon les données ou les commandes selon qu'elles sont entrées en majuscules ou en minuscules.


!!! info "Définition : Chemin absolu / Chemin relatif" 
        Pour naviguer dans le système de fichiers, nous devons définir les **chemins absolus** et les **chemins relatifs**.  
        - Un chemin d'accès **absolu** part toujours du répertoire racine `/` suivi de la suite du ou des nom(s) de répertoire(s) jusqu'à la destination, séparé(s) par des symboles `/`.  
        - Lorsqu'on se déplace dans l'arborescence de fichiers, le répertoire dans lequel on se situe s'appelle le **répertoire courant**. Un **chemin relatif** fait référence au répertoire courant et donne la liste des noms du ou des répertoire(s) jusqu'à la destination, séparés par des `/`.

!!! example "Exemple"

        - Exemple de chemin absolu vers le répertoire `Exos` de Matéi : `/home/Matéi/Math/Exos`
        - Exemple de chemin relatif vers le répertoire `Exos` de Matéi depuis le répertoire `home` : `Matéi/Math/Exos`

1. Ecrire le chemin absolu vers le répertoire `NSI` d'Angie.  
2. Ecrire le chemin relatif vers le répertoire `NSI` d'Angie à partir du répertoire courant `Angie`.  
3. Ecrire le chemin relatif vers le répertoire `Jeux` d'Angie à partir du répertoire courant `NSI`.  

    !!! warning "Remarque"

        Pour remonter d'un niveau dans l'arborescence de fichiers, il faut utiliser `..`.
        !!! example "Exemple"

            Exemple de chemin relatif du répertoire `Angie` au répertoire `Math` : `../Matéi/Math`

    Pour manipuler les commandes Linux, nous allons utiliser un émulateur d'ordinateur dans un navigateur web. L'**interface en ligne de commande** va nous permettre d'interagir avec un véritable système d'exploitation Linux.  

4. Aller sur [lycee.editions-bordas.fr/cahier-NSI1re](http://lycee.editions-bordas.fr/cahier-NSI1re){:target="_blank" }, puis cliquer sur *Séquence 6* et enfin sur *Emulateur Linux*. Attendre la demande de login, puis saisir « `Angie` » avec le mot de passe « `NSI` ».

    !!! info "Commande : `ls`"

        La commande **`ls`** (*list* en anglais) permet d'afficher la liste des répertoires et des fichiers dans le répertoire courant.

5. Saisir la commande `ls`. Commenter.

    !!! info "Commande : `pwd` et `cd`"

        La commande **`pwd`** (*print working directory* en anglais) affiche le répertoire courant.  
        La commande **`cd`** (*change directory* en anglais) permet de changer de répertoire courant.  
        
        
6. Tester la commande `pwd`, puis écrire et tester les commandes appropriées afin de vérifier vos réponses aux questions 2. et 3.

??? note pliée "Corrigé"
    1. `/home/Angie/NSI`
    2. `NSI`
    3. `../Jeux`
    4. Une fois saisis les identifiants on accède à l'émulateur Linux.
    5. La commande `ls` donne la liste des répertoires accessibles depuis le répertoire `Angie`.
    6.  `pwd` donne `/home/Angie`  
        `cd NSI`  
        `cd ../Jeux`  

## B. Commandes de base
Angie veut mettre de l'ordre dans son répertoire personnel. Son système de fichiers est le même que celui de la partie A.  

!!! info "Commande : `cat`"

        La commande __`cat nom_fichier`__  (*catenate* en anglais) affiche dans la console le contenu du fichier *nom_fichier* donné en argument.  

1. A l'aide de la commande `cd`, aller dans le répertoire `NSI`, puis afficher le contenu du fichier `Modules`. Ecrire les commandes saisies dans la console.  

    !!! info "Commande : `mkdir`"

        La commande **`mkdir nom_repertoire`** (*make directory* en anglais) permet de créer un répertoire nommé *nom_répertoire* dans le répertoire courant.    
        On peut également créer un répertoire dans un autre répertoire en précisant le chemin relatif ou absolu :  
        `mkdir chemin/nom_repertoire`.  
        
2. Créer le répertoire `OS` dans le répertoire `NSI` à l'aide de la commande `mkdir` et vérifier sa création avec la commande `ls`. Ecrire les commandes nécessaires ci-dessous.

3. Sans changer de répertoire courant, créer le répertoire `Linux` dans le répertoire `Angie`. Ecrire les commandes nécessaires ci-dessous.

    !!! info "Commande : `mv`"
        La commande `mv source destination` (*move* en l'anglais) permet de déplacer et/ou de renommer des fichiers et des répertoires. Les arguments `source` et `destination` sont constitués du nom du fichier ou du répertoire, précédés éventuellement du chemin.
        
4. À l'aide de la commande `cd`, aller dans le répertoire `Angie`. Déplacer le répertoire `Types` d'Angie dans le répertoire `NSI` à l'aide de la commande `mv`. Écrire les commandes nécessaires ci-dessous.

5. Quel est le résultat de la commande `mv Encodage NSI/Encodages` ?

    !!! info "Commande : `cp`"
        La commande `cp source destination` (*copy* en anglais) permet de copier des fichiers ou des répertoires `source` vers la `destination`.

6. Sans changer de répertoire courant, créer un répertoire `Programmation` dans le répertoire `NSI`, puis copier le fichier `Modules` dans le répertoire `Programmation`. Écrire les commandes nécessaires ci-dessous.

    !!! info "Commande : `rm`"
        La commande `rm chemin/nom_fichier` (*remove* en anglais) permet de supprimer un ou des fichier(s).
        

7. Supprimer le fichier `Modules` du répertoire `NSI`. Écrire la commande nécessaire ci-dessous.  

8. Tester la commande `rm Danse`. Que remarquez-vous ? Justifier.   

    !!! info "Commande : `man`"
        Pour avoir accès à la documentation d'une commande, il suffit de saisir
        `man nom_commande`.

9. Tester la commande `rm -r Danse`. Après avoir saisi `man rm`, conclure sur l'utilité de l'option `-r`.

??? note pliée "Corrigé"
    1. `cd /home/Angie/NSI` ou `cd NSI`  
        `cat Modules`
    2. `mkdir OS`  
        `ls`  
    3. `mkdir ../Linux`  
        ou `mkdir /home/Angie/Linux`  
        On peut vérifier avec les commandes `cd ..` et `ls`.
    4. `cd ..`  
        `mv Types NSI` 
    5. Le répertoire `Encodage` est déplacé dans le répertoire `NSI` et il est renommé `Encodages` (avec un *s* final)
    6.  `mkdir NSI/Programmation`  
        `cp NSI/Modules NSI/Programmation`  
    7. `rm NSI/Modules`
    8. La console affiche une erreur puisque `Danse` est un répertoire.
    9. Cette commande permet de supprimer le répertoire donné en argument, ainsi que tous ses sous-répertoires.  
    