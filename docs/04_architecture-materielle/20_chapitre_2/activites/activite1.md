---
author: Sophie CHANE-LUNE
title: Activité 1 Présentation
---

Regardez cette vidéo, puis répondez aux questions suivantes :
<div class="centre">
<iframe id='ivplayer' 
width='560' height='315' 
src='https://invidious.projectsegfau.lt/embed/4OhUDAtmAUo?t=9'
title="Vidéo Histoire des systèmes d'exploitation" 
frameborder="0" 
allow="accelerometer; autoplay; clipboard-write; encrypted-media; fullscreen; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
</div>

1. Quel est le rôle d’un système d’exploitation sur un ordinateur ?
2. Citez les systèmes d’exploitation les plus importants.
3. Quelles sont les fonctions d’un système d’exploitation ?
4. Complétez le schéma ci-dessous qui situe le système d’exploitation dans un ordinateur :
![paysage](../images/tab.svg){ width=25%; : .center }

??? note pliée "Corrigé"

    1. Un système d'exploitation est un intermédiaire entre le matériel physique et les applications. Son rôle est d'exploiter des ressources matérielles.

    2. Les systèmes d’exploitation les plus importants sont : Windows, Mac OS, Linux, iOS, Androïd
        ![Système d'exploitation](../images/OS_exemples.webp){ width=25%; : .center }

    3. Les fonctions d’un système d’exploitation sont :
        - gestion des fichiers
        - gestion de la mémoire
        - gestion des processus
        - gestion des entrées / sorties
        - gestion des communications réseau
        - interfaces graphiques / interactions

    4. ![paysage](../images/tab-solution.svg){ width=25%; : .center }  