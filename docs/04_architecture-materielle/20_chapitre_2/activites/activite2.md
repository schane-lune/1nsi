---
author: Sophie CHANE-LUNE
title: Activité 2 Logiciels libres, Logiciels propriétaires
---

## A. Définition
Regardez cette vidéo, puis répondez aux questions suivantes :
<iframe width="560" height="315" src="https://www.youtube.com/embed/hDkNHT59Jmg" title="Logiciel libre &amp; Open source" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


1. Qui est le créateur du mouvement du logiciel libre ?
2. Quelles sont les quatre libertés d’un logiciel libre ?
3. À quoi faut-il avoir accès pour pouvoir exercer ces libertés ?
4. Quel est le contraire d’un logiciel libre ?

??? note pliée "Corrigé"

    1. Le créateur du mouvement du logiciel libre est Richard Stallman. 
    ![paysage](../images/stallman.jpg){ width=25%; : .center }

    2. Les quatre libertés d’un logiciel libre sont : 
        - la liberté d'**utiliser** le logiciel
        - la liberté de **copier** le logiciel
        - la liberté d'**étudier** le logiciel
        - la liberté de **modifier/distribuer** le logiciel

    3. Il faut avoir accès au code source.

    4. Le contraire d’un logiciel libre est un logiciel privateur ou logiciel propriétaire.

## B. Logiciel libre ou logiciel propriétaire
!!! info "Définition : Logiciel libre" 
    D'après la Free Software Foundation, un logiciel **libre** (*free software* en anglais) désigne un logiciel qui respecte la liberté des utilisateurs. Ceux-ci ont la liberté d'exécuter, de copier, de distribuer, d'étudier, de modifier et enfin d'améliorer ce logiciel.  


!!! info "Définition : logiciel propriétaire"
    Un logiciel **propriétaire** (ou encore appelé **privateur**) est un logiciel qui est la propriété exclusive de la société qui l'a conçu et qui le commercialise. Celui-ci est distribué uniquement en version "exécutable", alors que les logiciels libres ont un "code source" entièrement accessible.  

1. Pensez-vous qu'un logiciel libre soit forcement gratuit ? Est-il distribuable gratuitement ?
2. Certaines applications comme Snapchat sur smartphone sont gratuites. Sont-elles pour autant libre ?

??? note pliée "Corrigé"
    1. Un logiciel libre n'est pas forcément gratuit mais l'accès à son code source est garanti et laisse la possibilité de le modifier et de le redistribuer librement.

    2. Non, une application peut être gratuite sans être libre.

## C. Système d'exploitation libre ou propriétaire ?
!!! info "Définition : Système d'exploitation" 
    Un **système d'exploitation**, souvent appelé OS (*Operating System* en anglais) est un logiciel qui permet à l'utilisateur d'accéder aux ressources d'un ordinateur et à ses périphériques. Il est également le "chef d'orchestre" de tous les programmes. Linux et GNU/Linux est une famille de systèmes d'exploitation libre et open source. Le système d'exploitation Windows est la propriété exclusive de Windows.  

1. Quelles sont les conséquences du statut propriétaire de Windows ?  

Dans l'image ci-dessous du réseau local de Pauline, tous les appareils informatiques ont un système d'exploitation Linux.  
![Pauline](../images/OS_appareil.png){ width=50%; : .center }
2. Pourquoi ces appareils nécessitent-ils un système d'exploitation ?  
3. Ces appareils ont-ils la même version du système d'exploitation ?  
4. Formuler une hypothèse sur la raison du choix de Linux par les différents fabricants.  

??? note pliée "Corrigé"
    1. Windows est un système d'exploitation que l'on ne peut ni modifier ni améliorer puisque l'on n'a pas accès à son code source. On ne peut pas non plus le copier ni le distribuer.

    2. Ces appareils utilisent des ressources matérielles, des périphériques d'entrée/sortie et des programmes, qui doivent être pilotés par un système d'xploitation.

    3. Tous ces appareils n'ont pas la même version du système d'exploitation, car ils n'ont pas les mêmes fonctions. Les fabricants ont donc adapté le système d'exploitation à leurs besoins.

    4. Les fabricants ont pu récupérer légalement et gratuitement le code source d'une version de Linux, le modifier selon leurs besoins et l'intégrer dans leurs appareils informatiques.