---
author: Sophie CHANE-LUNE
title: Activité 4 Etude des droits et des permissions d'accès
---

## A. Droits et permissions d'accès
![](../images/act4_arborescence.png){ width=40%; : .center }  
Angie s'intéresse aux droits et aux permissions d'accès à des fichiers contenant les astuces de son jeu vidéo préféré, enregistrées sur l'ordinateur familial qui a pour système d'exploitation Linux. Son système de fichiers est le même que celui de la partie A.  

1. Aller sur [lycee.editions-bordas.fr/cahier-NSI1re](http://lycee.editions-bordas.fr/cahier-NSI1re){:target="_blank" }, puis cliquer sur *Séquence 6* et enfin sur *Emulateur Linux*. Attendre la demande de login, puis saisir « `Angie` » avec le mot de passe « `NSI` ».  

    !!! info "Touche : `tab`"
        Quand on saisit un nom de fichier ou de répertoire, la touche `tab` permet de compléter automatiquement son nom.

2. À l'aide de la commande `cd`, aller dans le répertoire `Jeux` d'Angie.  

    !!! info "Commande : `ls -l`"
        L'option `-l` de la commande `ls` permet d'afficher plus d'informations, comme les droits et les permissions d'accès de tous les fichiers et les répertoires contenus dans le répertoire courant. Le premier caractère permet de différencier les répertoires des fichiers avec le caractère `d` (*directory* en anglais) pour les répertoires et le caractère `-` pour les fichiers.  

3. Saisir la commande `ls -l`, puis identifier les répertoires et les fichiers du répertoire d'Angie.  

    ![](../images/act4_droits.png){ width=40%; align=right }  

    Les caractères ci-contre correspondent aux droits d'accès et aux permissions.  
    

4. Après avoir utilisé la commande `ls -l`, recopier les dix premiers caractères affichés pour le fichier `Mondes` et pour le répertoire `Saisons`.

    La présence des lettres `r`, `w` et `x` donne le droit correspondant et `-` l'interdiction.

    ![](../images/act4_permissions.png){ width=85%; : .center }

    L'ordinateur familial est configuré pour qu'Angie et son frère Matéi soient dans le même groupe d'utilisateurs, appelé `enfants`. Leurs parents ont leur propre groupe, différent, appelé `parents`. Sur un fichier ou un répertoire d'Angie, les parents sont considérés comme « autres utilisateurs ». Enfin, Angie est également « propriétaire » sur ses propres fichiers et répertoires.  

5. Compléter le tableau suivant avec « oui » ou « non » selon les droits accordés aux utilisateurs.  
    ![](../images/act4_tableau.png){ width=85%; : .center }

6. Quelles actions le père d'Angie peut-il ou non effectuer sur le répertoire `Saisons` ?

??? note pliée "Corrigé"
    1. Une fois saisis les identifiants on accède une nouvelle fois à l'émulateur Linux.  
    2. `cd /home/Angie/`
    3. `Mondes` et `Personnages` sont des fichiers et `Saisons` est un répertoire.  
    4. Pour `Mondes`: `-rw-r--r--` et pour `Saisons`: `drwxr-xr--`.
    5. Voici le tableau :  
        ![](../images/act4_tableaucorrige.png){ width=70%; : .center }
    6. Le père d'Angie peut afficher la liste des fichiers et répertoires du répertoire `Saisons` mais ne peut ni supprimer ni changer le nom des fichiers qu'il contient, ni y créer de nouveaux fichers. Il ne peut pas non plus ouvrir le répertoire `Saisons` pour regarder ce qu'il contient.

## B. Modification des droits et des permissions d'accès
Angie veut donner plus de droits à son petit frère Matéi afin qu'il puisse avoir accès à certains fichiers contenant des astuces pour son jeu vidéo préféré et les modifier lui-même.  

1. L'arborescence interne de ce répertoire est représentée dans la partie A de cette activité. À l'aide de la commande `cd`, aller dans le répertoire `Jeux` d'Angie.   

    !!! info "Commande : `chmod`"
        La commande `chmod droits now` (*change mode* en anglais) permet de modifier les permissions d'accès d'un fichier ou d'un répertoire nommé `nom`. La syntaxe des `droits` est représentée ci-dessous. 
        
    Ainsi, par exemple, la commande `chmod ug+x nom_fichier` ajoute le droit d'exécution du fichier `nom_fichier` au propriétaire et aux utilisateurs faisant partie du groupe du fichier :  

    ![](../images/act4_modif-droits.png){ width=70%; : .center }  

2. À l'aide de la commande `ls -l`, recopier les droits du fichier `Personnages`, puis modifier ses droits afin qu'il soit modifiable par le frère d'Angie, qui est dans le même groupe que le propriétaire. Vérifier enfin que les droits ont bien été changés.  

3. Tester la commande `chmod u=rw,go=r Mondes`. Faire une hypothèse sur l'utilité de la virgule.  

    !!! info "Commande : `chmod -R`"
        Au lieu de modifier les fichiers un par un, Angie a découvert, en utilisant la commande `man chmod`, que l'option `-R` (*recursive* en anglais) modifie les autorisations de tout le contenu d'un répertoire.  


4. Tester, puis écrire la commande qui permet d'ajouter au répertoire `Saisons` et à tout ce qu'il contient les droits en lecture et en écriture au groupe du fichier. Supprimer les droits en lecture aux utilisateurs qui ne font pas partie du groupe.  

    !!! info "Modification des droits"
        Il est également possible de modifier les droits avec une valeur octale (base 8) de trois chiffres comme décrit dans le tableau suivant.  
        ![](../images/act4_chmod.png){ width=50%; : .center }  
        ![](../images/act4_chmod2.png){ width=50%; : .center }  

5. Se déplacer dans le répertoire `Saisons`, puis tester et écrire avec cette syntaxe la commande équivalente à :
    a. `chmod u=rw,go=r Saison1`  
    b. `chmod u=rw,g=r,o-rwx Saison2` 

??? note pliée "Corrigé"
    1. `cd /home/Angie/Jeux`  
    2. `ls -l` affiche les droits du fichier `Personnages` qui sont `-rw-r--r--`.  
        La commande `chmod g+w Personnages` permet de modifier les droits ainsi `-rw-rw-r--`  
    3. Cette commande donne uniquement l'autorisation de lecture et d'écriture au propriétaire du fichier `Mondes` et une autorisation de lecture du fichier   au groupe et aux autres utilisateurs.  
        La virgule permet de faire plusieurs modifications dans la même commande.  
    4. `chmod -R g+rw,o-r Saisons`
    5.  a. `chmod 644 Saison1`  
        b. `chmod 640 Saison2`  
        