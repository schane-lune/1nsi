---
author: Sophie CHANE-LUNE
title: Système d'exploitation - leçon
---
# SYSTÈME D'EXPLOITATION
## Introduction
Le **système d'exploitation** agit comme un intermédiaire entre le matériel et les logiciels, permettant ainsi aux utilisateurs d'interagir avec leur ordinateur de manière transparente et efficace tout en garantissant la stabilité et la sécurité du système.  

*REPÈRES HISTORIQUES*
<div class="centre">
<iframe id='ivplayer' 
width='560' height='315' 
src='https://invidious.projectsegfau.lt/embed/4OhUDAtmAUo?t=9'
title="Vidéo Histoire des systèmes d'exploitation" 
frameborder="0" 
allow="accelerometer; autoplay; clipboard-write; encrypted-media; fullscreen; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
</div>

## I. Les fonctions d'un système d'exploitation
!!! abstract "Définition : Système d'exploitation" 
    Le **système d'exploitation**, appelé OS (*Operating System* en anglais), est un logiciel essentiel qui agit comme une interface entre le matériel informatique (comme le processeur, la mémoire, les disques durs, etc.) et les applications logicielles. Son rôle est de gérer les ressources matérielles de l'ordinateur et de fournir un environnement d'exécution stable et convivial pour les utilisateurs et les programmes. Il est également le "chef d'orchestre" de tous les programmes.  
    Voici les principales fonctions d'un système d'exploitation :  
        - gestion des fichiers  
        - gestion de la mémoire  
        - gestion des processus  
        - gestion des périphériques (entrées / sorties)  
        - gestion des communications réseau  
        - interfaces graphiques / interactions  

## II. Le système de gestion de fichier

## III. Les commandes de base
Les principales commandes de base pour Linux sont :  

| Commande  |                           | Description                                          | Syntaxe                     |
| :-------: |:------------------------- | :----------------------------------------------------| :-------------------------- |
| `cd`      | *change directory*        | Changer de répertoire courant                        | `cd` *`nom_repertoire`*     |
| `ls`      | *list*                    | Lister le contenu d'un répertoire courant            | `ls` *`nom_repertoire`*     |
| `mkdir`   | *make directory*          | Créer un répertoire dans le répertoire courant       | `mkdir` *`nom_repertoire`*  |
| `cp`      | *copy*                    | Copier des fichiers ou des répertoires               | `cp`*`source destination`*  |
| `mv`      | *move*                    | Déplacer ou renommer des fichiers ou des répertoires | `mv` *`source destination`* |
| `pwd`     | *print working directory* | Afficher le nom du répertoire courant                | `pwd`                       |
| `rm`      | *remove*                  | Supprimer des fichiers ou des répertoires            | `rm` *`source`*             |
| `man`     | *manual*                  | Afficher la documentation d'une commande             | `man` *`nom_commande`*      |
| `cat`     | **                        | Afficher dans la console le contenu d'un fichier     | `cat` *`nom_fichier`*       |
| `touch`   | **                        | Créer un fichier vide ou réinitialiser le timestamp d’un fichier | `cat` *`nom_fichier`*       |
| `echo`    | **                        | Afficher un message ou le contenu d’une variable     | `echo` *`source`*       |


## IV. Les droits et permissions d'acès aux fichiers



## Conclusion
<iframe width="560" height="315" src="https://www.youtube.com/embed/YScMI8lsy9s" title="Un système d&#39;exploitation c&#39;est quoi? [Bases Informatique]" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

!!! tip "Ce qu'il faut savoir et savoir faire"

    - Identifier les fonctions d'un système d'exploitation  
    - Utiliser les commandes de base en ligne de commande  
    - Gérer les droits et permissions d'accès aux fichiers  

