---
author: Sophie CHANE-LUNE
title: Objectifs
---

# ARCHITECTURE MATÉRIELLE & SYSTÈME D'EXPLOITATION

1. [**Architecture matérielle**](./architecture-materielle.md)

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Modèle d’architecture séquentielle (von Neumann).
        - Distinguer les rôles et les caractéristiques des différents constituants d’une machine. 
        - Dérouler l’exécution d’une séquence d’instructions simples du type langage machine.

2. **Systèmes d'exploitation**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Systèmes d’exploitation.
        - Identifier les fonctions d’un système d’exploitation. 
        - Utiliser les commandes de base en ligne de commande. 
        - Gérer les droits et permissions d’accès aux fichiers.

3. **Architecture d'un réseau**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Transmission de données dans un réseau.
        - Protocoles de communication.
        - Architecture d’un réseau.
        - Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation. 
        - Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné). 
        - Simuler ou mettre en œuvre un réseau.

4. **IHM**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Périphériques d’entrée et de sortie.
        - Interface Homme-Machine (IHM).
        - Identifier le rôle des capteurs et actionneurs. 
        - Réaliser par programmation une IHM répondant à un cahier des charges donné


