---
author: Sophie CHANE-LUNE
title: Objectifs
---

# ALGORITHMIQUE

1. **Algorithme de parcours séquentiel**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Parcours séquentiel d’un tableau.
        - Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque. 
        - Écrire un algorithme de recherche d’un extremum, de calcul d’une moyenne.

2. **Algorithme de tri**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Tris par insertion, par sélection.
        - Écrire un algorithme de tri. 
        - Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection.

3. **Algorithme de recherche**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Recherche dichotomique dans un tableau trié.
        - Montrer la terminaison de la recherche dichotomique à l’aide d’un variant de boucle.

4. **Algorithmes gloutons**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Algorithmes gloutons.
        - Résoudre un problème grâce à un algorithme glouton.

5. **Algorithme des k plus proches voisins**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Algorithme des k plus proches voisins.
        - Écrire un algorithme qui prédit la classe d’un élément en fonction de la classe majoritaire de ses k plus proches voisins.