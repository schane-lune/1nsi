---
author: Sophie CHANE-LUNE
title: Objectifs
---

# TRAITEMENT DE DONNÉES EN TABLES

1. **Recherche dans une table**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Indexation de tables.
        - Importer une table depuis un fichier texte tabulé ou un fichier CSV.
        - Recherche dans une table.
        - Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle.

2. **Tri d'une table**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Tri d’une table.
        - Trier une table suivant une colonne.

3. **Fusion de tables**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Fusion de tables.
        - Construire une nouvelle table en combinant les données de deux tables.

