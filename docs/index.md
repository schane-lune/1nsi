# Lycée MHA

## Programme EDUSCOL
[Programme de NSI Première](https://eduscol.education.fr/document/30007/download){ .md-button target="_blank" rel="noopener"}

## Déroulement

1. [Architecture matérielle](./04_architecture-materielle/index.md)

2. [Initiation à la programmation](./05_langage-programmation/index.md)

3. [Représentation des nombres entiers positifs](./01_representation-donnees/representation-entier-positif.md)

4. Systèmes d'exploitation

5. Réseau

## Blocs utilisés
!!! abstract "Définition"

!!! example "Exemple"

!!! tip "Ce qu'il faut savoir et savoir faire"

!!! warning "Remarque/Attention"

!!! quote "Citation"

!!! history "Histoire de l'informatique"

!!! video "Video"

!!! info "Remarque"

!!! note "Exercice"

!!! check "Solution/Correction"

!!! gear "Méthode/algorithme"

!!! code "Code/Programme"

!!! lien "Lien externe"

!!! capytale "Lien vers activité Capytale"