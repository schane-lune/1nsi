---
author: Sophie CHANE-LUNE
title: Objectifs
---

# INTERACTIONS ENTRE L'HOMME ET LA MACHINE SUR LE WEB 

1. **Interaction entre l’homme et la machine**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Modalités de l’interaction entre l’homme et la machine.
        - Événements.
        - Identifier les différents composants graphiques permettant d’interagir avec une application Web. 
        - Identifier les événements que les fonctions associées aux différents composants graphiques sont capables de traiter. 

3. **Interaction avec une page web**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Interaction avec l’utilisateur dans une page Web
        - Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web.

        - Formulaire d’une page Web.
        - Analyser le fonctionnement d’un formulaire simple. 
        - Distinguer les transmissions de paramètres par les requêtes POST ou GET.

3. **Interaction client-serveur**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Interaction client-serveur. 
        - Requêtes HTTP, réponses du serveur.
        - Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre. 
        - Distinguer ce qui est mémorisé dans le client et retransmis au serveur. 
        - Reconnaître quand et pourquoi la transmission est chiffrée. 